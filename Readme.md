-> [System Integration with JBoss](https://developer.jboss.org/wiki/SystemIntegrationWithJBossFELCVUTJaro2017)

Projekt sa púšťa veľmi jednoducho nad pom.xml treba pustiť mvn clean camel:run alebo pustiť com.redhat.brq.integration.camel.MainCamel.java

endpoint je na adrese http:\\localhost:8080\orders -> pre vytvorenie objedávky, http:\\localhost:8080\orders\${orderid} pre get objednavky

* príklad validného post requestu:

```
#!json
{
	"address": {
		"firstName": "Jiri",
		"lastName": "Novak",
		"street": "Purkynova",
		"city": "Brno",
		"zipCode": "602 00"
	},
	"items": [
		{
			"itemName": "fedora",
			"count": 3
		}
	]
}
```

```
#!xml
<Order>
  <address>
		<firstName>Jiri</firstName>
		<lastName>Novak</lastName>
		<street>Purkynova</street>
		<city>Brno</city>
		<zipCode>602 00</zipCode>
	</address>
	<items>
		<itemName>fedora</itemName>
		<count>3</count>
	</items>
</Order>
```

# Buildenie  + vytvorenie docker image + spustenie #

* install projektu

```
#!maven
mvn clean install

```

* potom dáš v git root

```
#!docker
docker build -t lampedan/exam docker
```

* a potom to spustíš 

```
#!docker
docker run -it -p 8080:8080 lampedan/exam
```