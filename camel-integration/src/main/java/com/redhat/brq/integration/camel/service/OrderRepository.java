package com.redhat.brq.integration.camel.service;

import com.redhat.brq.integration.camel.exception.InvalidOrderException;
import com.redhat.brq.integration.camel.exception.SupplierFailedException;
import com.redhat.brq.integration.camel.model.Order;
import com.redhat.brq.integration.camel.model.accounting.AccountingOrder;
import com.redhat.brq.integration.camel.model.common.Item;
import org.apache.camel.ExchangeProperty;

import java.util.Map;
import java.util.TreeMap;

public final class OrderRepository
{

    private static final Map<Long, AccountingOrder> ACCOUNTING_ORDER = new TreeMap<>();
    private static final Map<Long, Order> ORDER = new TreeMap<>();
    private static long sequence;


    public static void create(Order order)  throws InvalidOrderException
    {
        order.setId(++sequence);
        order.checkValues();
        ACCOUNTING_ORDER.put(order.getId(), new AccountingOrder(order));
        ORDER.put(order.getId(), order);
    }


    public static AccountingOrder get(@ExchangeProperty("orderId") long id)
    {
        return ACCOUNTING_ORDER.get(id);
    }


    public static Order getOrder(@ExchangeProperty("orderId") long id)
    {
        return ORDER.get(id);
    }


    public static int updateAccountingOrder(@ExchangeProperty("orderId") long id, @ExchangeProperty("count") int count, @ExchangeProperty("sqlItem") Item sqlItem)
    {
        ACCOUNTING_ORDER.get(id).addItem(count, sqlItem.getPrice());

        return sqlItem.getCount() - count;
    }

    public static void clear()
    {
        ACCOUNTING_ORDER.clear();
        sequence = 0;
    }

    public static void set(@ExchangeProperty("orderId") long id, @ExchangeProperty("count") int count, @ExchangeProperty("supplierAPrice") int supAPrice, @ExchangeProperty("supplierBPrice") int supBPrice)
            throws SupplierFailedException
    {
        if(supAPrice != -1 && supAPrice <= supBPrice)
            ACCOUNTING_ORDER.get(id).addItem(count, supAPrice);
        else if(supBPrice != -1)
            ACCOUNTING_ORDER.get(id).addItem(count, supBPrice);
        else
        {
            throw new SupplierFailedException();
        }
    }

    private OrderRepository()
    {
    }

}
