package com.redhat.brq.integration.camel.model.suppliers;

import javax.xml.bind.annotation.*;

/**
 * Created by lamper on 7.6.2017.
 */
@XmlRootElement(name = "ItemReply")
public class SupplierAReply extends SupplierReply
{
    public SupplierAReply()
    {
        super();
    }


    public SupplierAReply(boolean available, int price)
    {
        super(available, price);
    }

}
