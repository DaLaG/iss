package com.redhat.brq.integration.camel;

import com.redhat.brq.integration.camel.exception.InvalidAccountingException;
import com.redhat.brq.integration.camel.exception.InvalidOrderException;
import com.redhat.brq.integration.camel.model.Order;
import com.redhat.brq.integration.camel.model.OrderItem;
import com.redhat.brq.integration.camel.model.accounting.AccountingOrder;
import com.redhat.brq.integration.camel.model.common.Item;
import com.redhat.brq.integration.camel.model.suppliers.ItemRequest;
import com.redhat.brq.integration.camel.model.suppliers.SupplierA;
import com.redhat.brq.integration.camel.model.suppliers.SupplierAReply;
import com.redhat.brq.integration.camel.model.suppliers.SupplierBReply;
import com.redhat.brq.integration.camel.service.OrderRepository;
import com.redhat.brq.integration.camel.service.OrderStatusProvider;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.camel.model.dataformat.SoapJaxbDataFormat;
import org.apache.camel.model.rest.RestBindingMode;
import org.apache.camel.processor.aggregate.AggregationStrategy;
import org.restlet.data.MediaType;
import org.restlet.data.Protocol;

import java.util.ArrayList;
import java.util.List;

public class OrderProcessRoute extends RouteBuilder
{

    @Override
    public void configure() throws Exception
    {

        restConfiguration()
                .scheme("https")
                .component("restlet") // use camel-restlet component as rest api provider
                .bindingMode(RestBindingMode.json_xml) // binding json to/from POJO (DTO)
                .port(8080)
                .endpointProperty("restletRealm", "#realm")
                .endpointProperty("sslContextParameters", "#sslContextParameters")

                .dataFormatProperty("prettyPrint", "true")
                .dataFormatProperty("include", "NON_NULL") //
                .dataFormatProperty("json.in.disableFeatures", "FAIL_ON_UNKNOWN_PROPERTIES");


        acceptOrders();

        newOrder();

        warehouse();

        myWarehouse();

        accounting();

        suppliers();

        supplierA();

        supplierB();

        confirm();
    }


    private void acceptOrders()
    {
        /* accept order and convert from json to java object */
        rest("/order").consumes("application/json").consumes("application/xml").produces("application/json")
                .post().type(Order.class).to("direct:new-order")
                .get("/{orderId}").outType(Order.class).to("direct:find-order");

        from("direct:find-order").id("find-order")
            .setProperty("orderId", simple("${header.orderId}"))
            .bean(OrderRepository.class, "get")
            .choice()
                .when(body().isNull()).setHeader(Exchange.HTTP_RESPONSE_CODE, constant(404))
            .end();
    }


    private void newOrder()
    {
        from("direct:new-order").id("new-order")
            .onException(InvalidOrderException.class).handled(true)
                .log("Invalid order")
                .removeHeaders("*")
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(400))
                .setHeader(Exchange.CONTENT_TYPE, constant(MediaType.TEXT_PLAIN))
                .setBody(constant("Some of values in order are wrong!"))
            .end()

            .log("AccountingOrder = ${body}")
            .bean(OrderRepository.class, "create")
            .to("seda:warehouse")
            .removeHeaders("*")
            .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(201))
            //.bean(OrderRepository.class, "get")
        //        .setBody(constant(null))
        ;
    }


    private void warehouse()
    {
        from("seda:warehouse").id("warehouse")
            .setProperty("orderId", simple("${body.id}"))
            .transform().simple("${body.items}")
            .split(body(), getAggregationStrategy())
                .convertBodyTo(OrderItem.class)
                .setProperty("item", simple("${body}"))
                .setProperty("count", simple("${body.count}"))
                .setBody(simple("SELECT * FROM ITEM WHERE ID=\'${property.item.itemName}\'"))
                .to("jdbc:dbcpDataSource")
                .split(body().tokenize("\n")).streaming().stopOnException()
                    .convertBodyTo(String.class)
                    .choice()
                        .when(exchange ->
                        {
                                Item i = Item.createItem((String) exchange.getIn().getBody());
                                exchange.setProperty("sqlItem", i);
                                OrderItem oi = (OrderItem) exchange.getProperty("item");
                                return i.getCount() >= oi.getCount();
                        })
                            .to("direct:myWarehouse")
                        .otherwise()
                            .to("direct:suppliers")
                    .end()
                .end()
                .log("From DB:\n${body}")
            .end()
            .log("Direct accounting")
            .to("direct:accounting");
    }

    private void myWarehouse()
    {
        from("direct:myWarehouse").id("myWarehouse")
            .bean(OrderRepository.class, "updateAccountingOrder")
            .setBody(simple("UPDATE ITEM SET COUNT = ${body} WHERE ID=\'${property.item.itemName}\'"))
            .to("jdbc:dbcpDataSource")
            .convertBodyTo(String.class)
            .log("Received DB response: ${body}")
            .process(new DBUpdateProcessResponse());
    }

    private void accounting()
    {
        from("direct:accounting").id("accounting")
            .onException(InvalidAccountingException.class).handled(true)
                .log("Invalid accounting for order ${property.orderId}")
                .bean(OrderStatusProvider.class, "accountingInvalid")
            .end()

            .bean(OrderRepository.class, "get")
            .choice()
                .when( exchange -> {
                        return !((AccountingOrder) exchange.getIn().getBody()).getItems().isEmpty();
                    })
                    .marshal().json(JsonLibrary.Jackson)
                    .removeHeaders("*")
                    .setHeader(Exchange.HTTP_METHOD, constant(org.apache.camel.component.http4.HttpMethods.POST))
                    .setHeader(Exchange.CONTENT_TYPE, constant(MediaType.APPLICATION_JSON))
                    .to("https4://192.168.99.100:8443/accounting/rest/accounting/invoice/issue"
                            + "?authMethod=Basic"
                            + "&authenticationPreemptive=true"
                            + "&authUsername=admin"
                            + "&authPassword=foo"
                            + "&sslContextParameters=#sslContextParameters"
                            + "&x509HostnameVerifier=hostnameVerifier"
                    )
                    .unmarshal().json(JsonLibrary.Jackson)
                    .log("Received Accounting response: ${body}")
                    .process(new AccountingResponseProcessor())
                .otherwise()
                    .throwException(new InvalidAccountingException())
                    .setBody(constant(null))
                .end();
    }

    private void suppliers()
    {
        from("direct:suppliers").id("suppliers")
            .log("Ask in parallel to suppliers")
            .multicast(new AggregationStrategy()
            {
                @Override public Exchange aggregate(Exchange oldExchange, Exchange newExchange)
                {
                    if(oldExchange == null)
                        return newExchange;

                    if(newExchange == null)
                        return oldExchange;

                    newExchange.getProperties().forEach( (s,o) -> oldExchange.setProperty(s,o) );
                    return oldExchange;
                }
            }).parallelProcessing()
                .to("direct:supplier-a", "direct:supplier-b")
            .end()
            .bean(OrderRepository.class,"set");

    }

    private void supplierA()
    {
        SoapJaxbDataFormat  jaxb = new SoapJaxbDataFormat ();
        jaxb.setContextPath("com.redhat.brq.integration.camel.model.suppliers");

        from("direct:supplier-a").id("supplier-a")
            .process(new Processor()
            {
                @Override public void process(Exchange exchange) throws Exception
                {
                    OrderItem oi = (OrderItem) exchange.getProperty("item");
                    exchange.getOut().setBody(new SupplierA(oi.getItemName(), oi.getCount()));
                }
            })
                .marshal(jaxb)
                .removeHeaders("*")
                .setHeader(Exchange.HTTP_METHOD, constant(org.apache.camel.component.http4.HttpMethods.POST))
                .setHeader(Exchange.CONTENT_TYPE, constant(MediaType.APPLICATION_XML))
                .log("Supplier-A requers ${body}")
                .to("https4://192.168.99.100:8443/supplier-a/SupplierAService"
                        + "?authMethod=Basic"
                        + "&authenticationPreemptive=true"
                        + "&authUsername=webuser"
                        + "&authPassword=JBoss.123"
                        + "&sslContextParameters=#sslContextParameters"
                        + "&x509HostnameVerifier=hostnameVerifier"
                )
                .convertBodyTo(String.class)
                .process(new SupplierProccesResponse())
                .choice()
                    .when(exchange -> ((SupplierAReply) exchange.getIn().getBody()).isAvailable())
                        .setProperty("supplierAPrice", simple("${body.price}"))
                    .otherwise()
                        .setProperty("supplierAPrice", constant("-1"))
                .end()
                .log("Supplier-A response ${body}");

    }

    private void supplierB()
    {
        SoapJaxbDataFormat  jaxb = new SoapJaxbDataFormat ();
        jaxb.setContextPath("com.redhat.brq.integration.camel.model.suppliers");

        from("direct:supplier-b").id("supplier-b")
                .process(new Processor()
                {
                    @Override public void process(Exchange exchange) throws Exception
                    {
                        OrderItem oi = (OrderItem) exchange.getProperty("item");
                        exchange.getOut().setBody(new ItemRequest(oi.getItemName(), oi.getCount()));
                    }
                })
                .marshal(jaxb)
                .removeHeaders("*")
                .setHeader(Exchange.HTTP_METHOD, constant(org.apache.camel.component.http4.HttpMethods.POST))
                .setHeader(Exchange.CONTENT_TYPE, constant(MediaType.APPLICATION_XML))
                .log("Supplier-B requers ${body}")
                .to("https4://192.168.99.100:8443/supplier-b/SupplierBService"
                        + "?sslContextParameters=#sslContextParameters"
                        + "&x509HostnameVerifier=hostnameVerifier"
                )
                .convertBodyTo(String.class)
                .process(new SupplierProccesResponse())
                .choice()
                    .when(exchange -> ((SupplierBReply) exchange.getIn().getBody()).isAvailable())
                        .setProperty("supplierBPrice", simple("${body.price}"))
                    .otherwise()
                        .setProperty("supplierBPrice", constant("-1"))
                .end()
                .log("Supplier-B response ${body}");
    }

    private void confirm()
    {
        from("direct:confirm").id("confirm")
            .bean(OrderStatusProvider.class, "confirm");
    }


    private AggregationStrategy getAggregationStrategy()
    {
        return new AggregationStrategy()
        {
            @Override public Exchange aggregate(Exchange oldExchange, Exchange newExchange)
            {
                if (oldExchange == null)
                {
                    List<OrderItem> orders = new ArrayList<OrderItem>();
                    OrderItem newOrder = newExchange.getIn().getBody(OrderItem.class);
                    orders.add(newOrder);
                    newExchange.getIn().setBody(orders);
                    return newExchange;
                }
                List<OrderItem> orders = oldExchange.getIn().getBody(List.class);
                OrderItem newOutOrder = newExchange.getIn().getBody(OrderItem.class);
                orders.add(newOutOrder);
                oldExchange.getIn().setBody(orders);
                return oldExchange;
            }
        };
    }
}
