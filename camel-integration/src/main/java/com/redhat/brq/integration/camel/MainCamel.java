package com.redhat.brq.integration.camel;

import org.apache.camel.spring.Main;

/**
 * Created by lamper on 8.6.2017.
 */
public class MainCamel
{
    Main main;

    public static void main(String[] args) throws Exception {
        MainCamel example = new MainCamel();
        example.boot();
    }

    public void boot() throws Exception {
        main = new Main();

        //main.addRouteBuilder(new OrderProcessRoute());

        System.out.println("Starting Camel. Use ctrl + c to terminate the JVM.\n");
        main.run();
    }

  }
