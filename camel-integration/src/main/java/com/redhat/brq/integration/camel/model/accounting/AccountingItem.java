package com.redhat.brq.integration.camel.model.accounting;


import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;
/**
 * Created by Daniel on 1.6.2017.
 */
public class AccountingItem
{
    private long articleId;

    private int count;

    private double unitPrice;


    public AccountingItem()
    {
    }


    public AccountingItem(long articleId, int count, double unitPrice)
    {
        this.articleId = articleId;
        this.count = count;
        this.unitPrice = unitPrice;
    }


    public long getArticleId()
    {
        return articleId;
    }


    public void setArticleId(long articleId)
    {
        this.articleId = articleId;
    }


    public int getCount()
    {
        return count;
    }


    public void setCount(int count)
    {
        this.count = count;
    }


    public double getUnitPrice()
    {
        return unitPrice;
    }


    public void setUnitPrice(double unitPrice)
    {
        this.unitPrice = unitPrice;
    }

    public void setPrice(double price)
    {

    }

    @Override
    public String toString() {
        return "AccountingItem [articleId=" + articleId + ", count=" + count + ", unitPrice=" + unitPrice + "]";
    }

}

