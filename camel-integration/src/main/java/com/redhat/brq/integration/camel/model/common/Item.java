package com.redhat.brq.integration.camel.model.common;

import java.math.BigDecimal;

/**
 * Created by Daniel on 5.6.2017.
 */
public class Item {

    private String id;


    private int price;


    private int count;

    public Item() {
        super();
    }

    public Item(final String id, final int price, final int count) {
        super();
        this.id = id;
        this.price = price;
        this.count = count;
    }

    public Item(final int price, final int count) {
        this(null, price, count);
    }


    public static Item createItem(String sqlRes)
    {
        String[] body = sqlRes.substring(2, sqlRes.length()-2).split(",");

        int unitPrice = (int) Double.parseDouble(body[2].split("=")[1]);

        int dbCount = Integer.parseInt(body[1].split("=")[1]);

        return new Item(body[0].split("=")[1], unitPrice, dbCount);
    }


    public int getPrice() {
        return price;
    }
    public void setPrice(final int price) {
        this.price = price;
    }
    public int getCount() {
        return count;
    }
    public void setCount(final int count) {
        this.count = count;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Item [id=" + id + ", price=" + price + ", count=" + count
                + "]";
    }
}
