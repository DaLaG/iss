package com.redhat.brq.integration.camel.model;

import com.redhat.brq.integration.camel.exception.InvalidOrderException;

/**
 * Created by Daniel on 1.6.2017.
 */
public class OrderItem
{
    private String itemName;
    private int count;


    public OrderItem()
    {
    }


    public OrderItem(String itemName, int count)
    {
        this.itemName = itemName;
        this.count = count;
    }


    public String getItemName()
    {
        return itemName;
    }


    public void setItemName(String itemName)
    {
        this.itemName = itemName;
    }


    public int getCount()
    {
        return count;
    }


    public void setCount(int count)
    {
        this.count = count;
    }


    @Override
    public String toString()
    {
        return "OrderItem [itemName=" + itemName + ", count=" + count + "]";
    }


    public void checkValues() throws InvalidOrderException
    {
        if(itemName.trim().equals(""))
            throw new InvalidOrderException();


        if(count <= 0)
            throw new InvalidOrderException();
    }
}
