package com.redhat.brq.integration.camel.model.accounting;

import com.redhat.brq.integration.camel.model.Address;
import com.redhat.brq.integration.camel.model.Order;

import java.util.LinkedList;
import java.util.List;

public class AccountingOrder
{

    private long id;
    private List<AccountingItem> items;
    private Address address;

    private long sequence;

    public AccountingOrder()
    {
    }


    public AccountingOrder(Order order)
    {
        this.id = order.getId();
        this.items = new LinkedList<>();
        this.address = order.getAddress();
    }


    public long getId()
    {
        return id;
    }


    public void setId(long id)
    {
        this.id = id;
    }


    public Address getAddress()
    {
        return address;
    }


    public void setAddress(Address address)
    {
        this.address = address;
    }


    public List<AccountingItem> getItems()
    {
        return items;
    }


    public void setItems(List<AccountingItem> items)
    {
        this.items = items;
    }


    public void addItem(int count, double unitPrice )
    {
        items.add(new AccountingItem(++sequence, count, unitPrice));
    }


    public void setTotalPrice(double totalPrice) {

    }

    @Override
    public String toString()
    {
        return "AccountingOrder [id=" + id + ", items=" + items + ", address=" + address + "]";
    }

}
