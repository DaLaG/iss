package com.redhat.brq.integration.camel.model;

import com.redhat.brq.integration.camel.exception.InvalidOrderException;
import com.redhat.brq.integration.camel.model.OrderItem;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "Order")
@XmlAccessorType(XmlAccessType.FIELD)
public class Order
{
    @XmlElement
    private long id;

    @XmlElement
    private List<OrderItem> items;

    @XmlElement
    private Address address;

    @XmlElement
    private OrderStatus status;


    public Order()
    {
    }


    public Order(long id, List<OrderItem> items, Address address, OrderStatus status)
    {
        this.id = id;
        this.items = items;
        this.address = address;
        this.status = status;
    }


    public long getId()
    {
        return id;
    }


    public void setId(long id)
    {
        this.id = id;
    }


    public List<OrderItem> getItems()
    {
        return items;
    }


    public void setItems(List<OrderItem> items)
    {
        this.items = items;
    }


    public Address getAddress()
    {
        return address;
    }


    public void setAddress(Address address)
    {
        this.address = address;
    }


    public OrderStatus getStatus()
    {
        return status;
    }


    public void setStatus(OrderStatus status)
    {
        this.status = status;
    }


    @Override
    public String toString()
    {
        return "Order [id=" + id + ", items=" + items + ", address=" + address + "]";
    }


    public void checkValues() throws InvalidOrderException
    {
        for(OrderItem i : items)
            i.checkValues();
    }
}
