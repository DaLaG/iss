package com.redhat.brq.integration.camel.model.suppliers;

import javax.xml.bind.annotation.*;

/**
 * Created by lamper on 7.6.2017.
 */
@XmlRootElement(name="ItemReply", namespace = "urn:com.redhat.sysint.exam:supplier-b")
public class SupplierBReply extends SupplierReply
{
    public SupplierBReply()
    {
        super();
    }

    public SupplierBReply(boolean available, int price)
    {
        super(available,price);
    }
}
