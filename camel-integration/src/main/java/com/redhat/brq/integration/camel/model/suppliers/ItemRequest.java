package com.redhat.brq.integration.camel.model.suppliers;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by Daniel on 6.6.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ItemRequest", namespace = "urn:com.redhat.sysint.exam:supplier-b")
public class ItemRequest
{
    @XmlElement
    private String sku;

    @XmlElement
    private int amount;


    public ItemRequest()
    {
    }


    public ItemRequest(String sku, int amount)
    {
        this.sku = sku;
        this.amount = amount;
    }


    public int getAmount()
    {
        return amount;
    }


    public void setAmount(int amount)
    {
        this.amount = amount;
    }


    public String getSku()
    {
        return sku;
    }


    public void setSku(String sku)
    {
        this.sku = sku;
    }


    @Override public String toString()
    {
        return "ItemRequest [sku=" + sku + ", amount=" + amount + "]";
    }

}
