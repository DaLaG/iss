package com.redhat.brq.integration.camel;

import com.redhat.brq.integration.camel.exception.SupplierFailedException;
import com.redhat.brq.integration.camel.model.suppliers.SupplierAReply;
import com.redhat.brq.integration.camel.model.suppliers.SupplierBReply;
import com.redhat.brq.integration.camel.model.suppliers.SupplierReply;
import org.apache.camel.Exchange;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;

/**
 * Created by lamper on 7.6.2017.
 */
public class SupplierProccesResponse implements org.apache.camel.Processor
{
    @Override public void process(Exchange exchange) throws Exception
    {

        String s = exchange.getIn().getBody(String.class);
        JAXBContext jaxbContext;

        if (s.contains("<ItemReply>"))
        {
            s = s.substring(s.indexOf("<ItemReply>"), s.indexOf("</ItemReply>") + "</ItemReply>".length());
            jaxbContext = JAXBContext.newInstance(SupplierAReply.class);
        }
        else if (s.contains("<ns2:ItemReply"))
        {
            s = s.substring(s.indexOf("<ns2:ItemReply"), s.indexOf("</ns2:ItemReply>") + "</ns2:ItemReply>".length());
            jaxbContext = JAXBContext.newInstance(SupplierBReply.class);
        }
        else
            throw new SupplierFailedException();


        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

        StringReader reader = new StringReader(s);
        SupplierReply item = (SupplierReply) unmarshaller.unmarshal(reader);

        exchange.getOut().setBody(item);
    }
}
