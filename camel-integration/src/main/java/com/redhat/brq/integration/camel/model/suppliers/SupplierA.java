package com.redhat.brq.integration.camel.model.suppliers;

import javax.xml.bind.annotation.*;

/**
 * Created by Daniel on 6.6.2017.
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "available", namespace = "urn:com.redhat.sysint.exam:supplier-a")
public class SupplierA
{
    @XmlElement(name = "ItemRequest")
    private ItemRequest itemRequest;

    public SupplierA()
    {

    }

    public SupplierA(String sku, int amount)
    {
        this.itemRequest = new ItemRequest(sku, amount);
    }


    public ItemRequest getItemRequest()
    {
        return itemRequest;
    }


    public void setItemRequest(ItemRequest itemRequest)
    {
        this.itemRequest = itemRequest;
    }

    @Override public String toString()
    {
        return "SupplierA [itemRequest=" + itemRequest.toString() + "]";
    }
}
