package com.redhat.brq.integration.camel.model.suppliers;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by lamper on 7.6.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public abstract class SupplierReply
{
    @XmlElement
    private boolean available;

    @XmlElement
    private int price;

    public SupplierReply()
    {
    }

    public SupplierReply(boolean available, int price)
    {
        this.available = available;
        this.price = price;
    }


    public int getPrice()
    {
        return price;
    }


    public void setPrice(int price)
    {
        this.price = price;
    }


    public boolean isAvailable()
    {
        return available;
    }


    public void setAvailable(boolean available)
    {
        this.available = available;
    }
}
