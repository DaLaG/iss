package com.redhat.brq.integration.camel;

import com.redhat.brq.integration.camel.exception.DBUpdateException;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;

/**
 * Created by Daniel on 5.6.2017.
 */
public class DBUpdateProcessResponse implements Processor
{
    @Override public void process(Exchange exchange) throws Exception
    {
        if (exchange.getIn().getBody() != null) {
            throw new DBUpdateException();
        }
    }
}
