package com.redhat.brq.integration.camel.test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import org.apache.camel.CamelContext;
import org.apache.camel.EndpointInject;
import org.apache.camel.Exchange;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.main.MainSupport.Option;
import org.apache.camel.model.ModelCamelContext;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.annotation.ComponentScan.Filter;
//import org.ops4j.pax.exam.Configuration;
//import org.ops4j.pax.exam.Option;
//import org.ops4j.pax.exam.junit.PaxExam;
//import org.ops4j.pax.exam.karaf.options.KarafDistributionBaseConfigurationOption;
//import org.ops4j.pax.exam.karaf.options.KarafDistributionKitConfigurationOption;
//import org.ops4j.pax.exam.spi.reactors.ExamReactorStrategy;
//import org.ops4j.pax.exam.spi.reactors.PerClass;
//import org.ops4j.pax.exam.util.Filter;
//import org.osgi.framework.BundleContext;
import org.springframework.context.annotation.Configuration;

import com.redhat.brq.integration.camel.model.Address;
import com.redhat.brq.integration.camel.model.Order;
import com.redhat.brq.integration.camel.model.OrderItem;
import com.redhat.brq.integration.camel.service.OrderRepository;

//import static org.ops4j.pax.exam.CoreOptions.bundle;
//import static org.ops4j.pax.exam.karaf.options.KarafDistributionOption.editConfigurationFileExtend;
//import static org.ops4j.pax.exam.karaf.options.KarafDistributionOption.editConfigurationFilePut;
//import static org.ops4j.pax.exam.karaf.options.KarafDistributionOption.karafDistributionConfiguration;
//import static org.ops4j.pax.exam.karaf.options.KarafDistributionOption.keepRuntimeFolder;
//import static org.ops4j.pax.exam.karaf.options.KarafDistributionOption.replaceConfigurationFile;

// TASK-10
// Finish integration test.
// Add @RunWith PaxExam class to run test inside JBoss Fuse with PaxExam
//@RunWith(PaxExam.class) // Run test inside jboss-fuse with PaxExam
//@ExamReactorStrategy(PerClass.class) // Alternatively PerMethod (Fuse started / stopped for each test method)

//@RunWith(CamelSpringJUnit4ClassRunner.class)
//@ContextConfiguration(classes = {FilterTest.ContextConfig.class}, loader = CamelSpringDelegatingTestContextLoader.class)
public class IntegrationTest extends CamelTestSupport {

    public static final String FUSE_PATH = System.getProperty("fuse.zip.url", null);
    public static final String ENDPOINT_FILE_BASEURL = System.getProperty("endpoint.file.baseUrl", null);

    public static final String KARAF_VERSION = "2.4.0"; // Karaf version in JBoss Fuse 6.2

    /**
     * Configure pax exam test framework. <br/>
     * <p/>
     * Check PaxExam documentation on <a href="https://ops4j1.jira.com/wiki/display/PAXEXAM3/Karaf+Test+Container+Reference#KarafTestContainerReference-replaceConfigurationFile">
     * OPS4J Wiki</a>.
     *
     * @return PaxExam configuration
     */
//    @Configuration
    public Option[] configure() {

        if(ENDPOINT_FILE_BASEURL == null) {
            throw new IllegalArgumentException(String.format("Property '%s' must be defined.", "FILE"));
        }
		return null;
    }


    
    // Filter in case there is more Camel contexts deployed
    // Make sure, that name in this filter matches id="camel" in spring/blueprint camelContext tag
//    @Filter("(camel.context.name=camel)")
    @Inject
    CamelContext context;

    @Inject
//    BundleContext bundleContext; // OSGI bundle context

    @EndpointInject(uri = "direct:new-order")
    private ProducerTemplate producer;

    @EndpointInject(uri = "mock:new-order-resp")
    private MockEndpoint response;

    @Override
    protected void doPreSetup() throws Exception {
//        assertNotNull("Camel context should exist", context);
//        assertNotNull("Bundle context should exist", bundleContext);
    }

    @Override
    protected CamelContext createCamelContext() throws Exception {
        return context;
    }

    @Override
    public boolean isCreateCamelContextPerClass() {
        // we override this method and return true, to tell Camel test-kit that it should only create CamelContext
        // once (per class), so we will re-use the CamelContext between each test method in this class
        return true;
    }

    @Before
    public void before() {
        // @Before methods are invoked inside container with access to services like bundle context
        System.out.println("\n@Before\n");
//        System.out.println("BundleContext: " + bundleContext);
    }

    @Test
    public void itTest() throws Exception {
        context.getRouteDefinition("new-order").adviceWith((ModelCamelContext) context, new RouteBuilder() {
            @Override
            public void configure() throws Exception {
                interceptSendToEndpoint("seda:issue-order")
                    .to("mock:new-order-resp");
            }
        });

        response.expectedMessageCount(1);
        // TASK-10: HTTP response code 201 is expected
        // TASK-10: Location header is expected to be '/orders/1'
        response.expectedHeaderReceived(Exchange.HTTP_RESPONSE_CODE, 201);
        response.expectedHeaderReceived("Location", "/orders/1");

        // TASK-10: send body - use createOrder() method to create body
        producer.sendBody(createOrder());

        Thread.sleep(TimeUnit.SECONDS.toMillis(20)); // wait for process

        response.assertIsSatisfied();
        assertNotNull(OrderRepository.get(1L));
        assertEquals("CONFIRMED", OrderRepository.getOrder(1L).getStatus().getResolution());
    }

    private Order createOrder() {
        List<OrderItem> items = new ArrayList<>(2);
        items.add(new OrderItem("fedora", 2));
        items.add(new OrderItem("fedora", 20));
        return new Order(1L, items, new Address("Jan", "Novak", "Purkynova", "Brno", "61200"), null);
    }
}
