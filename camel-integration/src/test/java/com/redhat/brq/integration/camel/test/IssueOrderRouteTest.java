package com.redhat.brq.integration.camel.test;

import java.io.File;

import org.apache.camel.EndpointInject;
import org.apache.camel.ProducerTemplate;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.redhat.brq.integration.camel.service.OrderRepository;

public class IssueOrderRouteTest extends OrderProcessRouteTest {

    @EndpointInject(uri = "seda:issue-order")
    private ProducerTemplate producer;

    @Before
    @After
    public void cleanInbox() {
        deleteDirectory("inbox");
    }

    @Test
    public void testIssueOrder() throws Exception {
        producer.sendBody(OrderRepository.get(1L));

        // wait a while to let the file be created
        Thread.sleep(2000);

        assertNotNull(OrderRepository.getOrder(1L));
        assertNotNull(OrderRepository.get(1L));
    }

    @Override
    protected String getTestedRouteName() {
        return "new-order";
    }
}
